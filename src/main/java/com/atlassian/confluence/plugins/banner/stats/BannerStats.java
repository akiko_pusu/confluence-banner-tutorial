package com.atlassian.confluence.plugins.banner.stats;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BannerStats
{
    @XmlElement
    private final int likes;
    @XmlElement
    private final int comments;
    @XmlElement
    private final int versions;
    @XmlElement
    private final int attachments;

    public BannerStats(final int likes, final int comments, final int versions, int attachments)
    {
        this.likes = likes;
        this.comments = comments;
        this.versions = versions;
        this.attachments = attachments;
    }

    public int getLikes()
    {
        return likes;
    }

    public int getComments()
    {
        return comments;
    }

    public int getVersions()
    {
        return versions;
    }

    public int getAttachments() { return attachments; }
}